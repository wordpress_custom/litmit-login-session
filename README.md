# Litmit login session

- Set limit session for each user and all users

# Install plug-in in Wordpress

- Login Admin
- Plugins => Add New => Upload plug-in
- Active

# Setting

- Global: 
	- Go to settings -> general
	![alt text](https://i.gyazo.com/15b3d1c73eb584571c300b4d953e9652.png)
	
- Each user:
	- Go to Users -> All Users -> Chose 1 user and click Edit
	![alt text](https://i.gyazo.com/a49207354eaf6c00a2d50dfe410e9117.png)