<?php
/*
Plugin Name: Limit Device Login
Version: 1.0.0
Author: Phuc Thai Company
Description: Manage device login
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Limit Login Sessions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Limit Login Sessions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License (http://www.gnu.org/licenses/gpl-2.0.html)
 for more details.

*/


/**
 * add option into admin setting (general page)
*/
add_action('admin_init', 'add_option_max_device_login');
function add_option_max_device_login(){
    $args_max_device_login = array(
        'type' => 'integer',
        'description' => 'Enter default maximum device login',
        'sanitize_callback' => 'sanitize_text_field'
    );
    register_setting('general', 'max_device_login_setting_admin', $args_max_device_login);

    add_settings_field(
        'max_device_login_setting_admin',
        'Max device login default',
        'add_option_max_device_login_callback_function',
        'general',
        'default',
        array( 'label_for' => 'max_device_login_setting_admin' )
    );

    $args_max_time_allow_login = array(
        'type' => 'integer',
        'description' => 'Enter default maximum device login',
        'sanitize_callback' => 'sanitize_text_field'
    );
    register_setting('general', 'max_time_allow_login_setting_admin', $args_max_device_login);

    add_settings_field(
        'max_time_allow_login_setting_admin',
        'Max time allow login default (hour)',
        'add_option_max_time_allow_login_callback_function',
        'general',
        'default',
        array( 'label_for' => 'max_time_allow_login_setting_admin' )
    );
}

/**
 * function callback add option max_device_login_setting_admin of action above
 */
function add_option_max_device_login_callback_function($args){
    echo '<input type="number" id="max_device_login_setting_admin" name="max_device_login_setting_admin" value="'. get_option('max_device_login_setting_admin') .'" />';
}

/**
 * function callback add option max_time_allow_login_setting_admin of action above
 */
function add_option_max_time_allow_login_callback_function($args){
    echo '<input type="number" id="max_time_allow_login_setting_admin" name="max_time_allow_login_setting_admin" value="'. get_option('max_time_allow_login_setting_admin') .'" />';
}


// add option in database
add_option('max_device_login_setting_admin', '5', 'Enter default maximum device login', 'yes');
add_option('max_time_allow_login_setting_admin', '720', 'Enter default expire login time of devices', 'yes');


add_filter('authenticate', 'lls_authenticate', 1000, 2);

function lls_authenticate($user, $username){

    // default parameters
    $max_device_login_setting = 0;
    $max_device_login_user = 0;

    // check user role. If have role => user exist => get max device can login
    if (isset($user->caps)) {
        $data_user = get_user_meta($user->ID);
        $max_device_login_setting = get_option('max_device_login_setting_admin');
        $max_device_login_user = $max_device_login_setting;

        if ($data_user['max_device_login'][0]) {
            $max_device_login_user = $data_user['max_device_login'][0];
        }
    }

    //echo '<p>Default max device: '. $max_device_login_setting .'</p>';
    //echo '<p>User max device: '. $max_device_login_user .'</p>';

    //die;
    //if(!username_exists($username) || !$user = get_user_by('login', $username))
        //return null; // will trigger WP default no username/password matched error

    // setup vars
    $max_sessions = $max_device_login_user;
    $max_oldest_allowed_session_hours = get_option('max_time_allow_login_setting_admin');
    $error_code = 'max_session_reached';
    $error_message = "Maximum $max_sessions login sessions are allowed. Please contact site administrator.";

    // 1. Get all active session for this user
    $manager = WP_Session_Tokens::get_instance( $user->ID );
    $sessions =  $manager->get_all();

    // 2. Count all active session
    $session_count = count($sessions);

    // 3. Return okay if active session less then $max_sessions
    if($session_count < $max_sessions)
        return $user;

    $oldest_activity_session = lls_get_oldest_activity_session($sessions);

    // 4. If active sessions is equal to 5 then check if a session has no activity last 4 hours
    // 5. if oldest session have activity return error
    if(
        ( $session_count >= $max_sessions && !$oldest_activity_session ) // if no oldest is found do not allow
        || ( $session_count >= $max_sessions && $oldest_activity_session['last_activity'] + $max_oldest_allowed_session_hours * HOUR_IN_SECONDS > time())
    ){
        return new WP_Error($error_code, $error_message);
    }

    // 5. Oldest activity session doesn't have activity is given recent hours
    // destroy oldest active session and authenticate the user

    $verifier = lls_get_verifier_by_session($oldest_activity_session, $user->ID);

    lls_destroy_session($verifier, $user->ID);

    return $user;

}

function lls_destroy_session($verifier, $user_id){

    $sessions = get_user_meta( $user_id, 'session_tokens', true );

    if(!isset($sessions[$verifier]))
        return true;

    unset($sessions[$verifier]);

    if(!empty($sessions)){
        update_user_meta( $user_id, 'session_tokens', $sessions );
        return true;
    }

    delete_user_meta( $user_id, 'session_tokens');
    return true;

}

function lls_get_verifier_by_session($session, $user_id = null){

    if(!$user_id)
        $user_id = get_current_user_id();

    $session_string = implode(',', $session);
    $sessions = get_user_meta( $user_id, 'session_tokens', true );

    if(empty($sessions))
        return false;

    foreach($sessions as $verifier => $sess){
        $sess_string = implode(',', $sess);

        if($session_string == $sess_string)
            return $verifier;

    }

    return false;
}


function lls_get_oldest_activity_session($sessions){
    $sess = false;

    foreach($sessions as $session){

        if(!isset($session['last_activity']))
            continue;

        if(!$sess){
            $sess = $session;
            continue;
        }

        if($sess['last_activity'] > $session['last_activity'])
            $sess = $session;

    }

    return $sess;
}

// add a new key to session token array

add_filter('attach_session_information', 'lls_attach_session_information');

function lls_attach_session_information($session){
    $session['last_activity'] = time();
    return $session;
}

add_action('template_redirect', 'lls_update_session_last_activity');

function lls_update_session_last_activity(){

    if(!is_user_logged_in())
        return;

    // get the login cookie from browser
    $logged_in_cookie = $_COOKIE[LOGGED_IN_COOKIE];

    // check for valid auth cookie
    if( !$cookie_element = wp_parse_auth_cookie($logged_in_cookie) )
        return;

    // get the current session
    $manager = WP_Session_Tokens::get_instance( get_current_user_id() );

    $current_session = $manager->get($cookie_element['token']);

    if(
        $current_session['expiration'] <= time() // only update if session is not expired
        || ( $current_session['last_activity'] + 5 * MINUTE_IN_SECONDS ) > time() // only update in every 5 min to reduce db load
    ){
        return;
    }

    $current_session['last_activity'] = time();
    $manager->update($cookie_element['token'], $current_session);

}



//////////////////////////



/**
 * Add the field on edit user screen.
 *
 * @param $user WP_User user object
 */
function wz_usermeta_form_field_max_device_login($user)
{
    $user_panel = wp_get_current_user();

    if ($user_panel->has_cap('administrator')) :
    ?>
    <h3>Maximum device login</h3>
    <table class="form-table">
        <tr>
            <th>
                <label for="max_device_login">Max device login</label>
            </th>
            <td>
                <input type="number"
                       class="regular-text ltr"
                       id="max_device_login"
                       name="max_device_login"
                       value="<?php echo esc_attr(get_user_meta($user->ID, 'max_device_login', true)); ?>"
                       title="Enter number maximum device.">
                <p class="description">
                    Please enter number maximum device.
                </p>
            </td>
        </tr>
    </table>
    <?php
    endif;
}

/**
 * The save action.
 *
 * @param $user_id int the ID of the current user.
 *
 * @return bool Meta ID if the key didn't exist, true on successful update, false on failure.
 */
function wz_usermeta_form_field_max_device_login_update($user_id)
{
    // check that the current user have the capability to edit the $user_id
    if (!current_user_can('edit_user', $user_id)) {
        return false;
    }

    // create/update user meta for the $user_id
    return update_user_meta(
        $user_id,
        'max_device_login',
        $_POST['max_device_login']
    );
}

// add the field to user's own profile editing screen
add_action(
    'edit_user_profile',
    'wz_usermeta_form_field_max_device_login'
);

// add the field to user profile editing screen
add_action(
    'show_user_profile',
    'wz_usermeta_form_field_max_device_login'
);

// add the save action to user's own profile editing screen update
add_action(
    'personal_options_update',
    'wz_usermeta_form_field_max_device_login_update'
);

// add the save action to user profile editing screen update
add_action(
    'edit_user_profile_update',
    'wz_usermeta_form_field_max_device_login_update'
);